// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.6;

// BEP20 Interface
interface iBEP20 {
    function transfer(address, uint) external returns (bool);
    function transferFrom(address, address, uint) external returns (bool);
    function approve(address, uint256) external returns (bool);
}

interface iFactory {
    function getPair(address, address) external returns (address);
}

interface iRouter {
    function swapExactTokensForTokens(uint, uint, address[] memory, address, uint) external returns (uint[] memory);
}

contract techSpikeAprE {
    address public owner;
    address public WBNB = 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c;          //mainnet contract address
    address public BUSD = 0x55d398326f99059fF775485246999027B3197955;          //mainnet contract address
    address public FACTORY = 0xBCfCcbde45cE874adCB698cC183deBcF17952812;       //mainnet pancake router
    address public ROUTER = 0x05fF2B0DB69458A0750badebc4f9e13aDd608C7F;        //mainnet pancake router
    address public _feeAccount = 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c;
    
    mapping(bytes32 => bool) public jobExists;
    mapping(bytes32 => bool) public jobReleased;
    mapping(bytes32 => uint) public mapJobToAmount;
    mapping(bytes32 => address) public mapJobToSender;
       
    event Deposit(address indexed sender, uint value, bytes32 JOBID);
    event Release(address indexed sender, uint value, bytes32 JOBID);

    constructor() {
        owner = msg.sender;
    }

    // Sender to send asset with JOBID 
    function deposit(address asset, uint value, bytes32 JOBID) public {
        //checks
        require(value > 0, "No Val");
        require(!jobExists[JOBID], "JOBID Exists");

        //receive
        iBEP20(asset).transferFrom(msg.sender, address(this), value);

        //jobID switch
        jobExists[JOBID] = true;

        //PancakeSwap to BUSD
        address pair1 = iFactory(FACTORY).getPair(asset, WBNB);
        address pair2 = iFactory(FACTORY).getPair(WBNB, BUSD);
        
        iBEP20(asset).approve(ROUTER, value);

        uint amountOutMin = 1;
        address[] memory path = new address[](2);
        path[0] = pair1;
        path[1] = pair2;
        uint deadline = block.timestamp + 900; // 15 mins
        uint[] memory _amounts = iRouter(ROUTER).swapExactTokensForTokens(value, amountOutMin, path, address(this), deadline);
        uint _finalBUSD = _amounts[1];
        
        //mappings
        mapJobToAmount[JOBID] = _finalBUSD;
        mapJobToSender[JOBID] = msg.sender;
          
        //emits event
        emit Deposit(msg.sender, _finalBUSD, JOBID);
    }

    // Sender to instruct release
    function release (bytes32 JOBID) public {
        require(jobExists[JOBID], "!JOBID");
        require(mapJobToSender[JOBID] == msg.sender, "!sender");
        require(!jobReleased[JOBID], "Job Released");

        jobReleased[JOBID] = true;
        
        uint _amount = mapJobToAmount[JOBID];
      
        _releaseWithFee(msg.sender, _amount);

        emit Release(msg.sender, _amount, JOBID);
    }

    // Transfers 95% to original sender account 
    // Transfers 5% to a fee account
    function _releaseWithFee(address _recipient, uint _amount) internal {
        uint _senderAmount = (_amount * 100)/95;
        uint _feeAmount = (_amount * 100)/5;

        iBEP20(BUSD).transfer(_recipient, _senderAmount); //95%
        iBEP20(BUSD).transfer(_feeAccount, _feeAmount); //5%
    }
}